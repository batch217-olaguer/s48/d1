//console.log(`Was up bro!`)

let posts = [];
let count = 0;

// add post data.

document.querySelector("#form-add-post"/*either id or class*/).addEventListener('submit', (event) => {

	// this line of code prevents page from refreshing
	event.preventDefault();

	posts.push({
		id: count,
		title: document.querySelector("#txt-title").value,
		body: document.querySelector("#txt-body").value
	});

	count++;
	// every post new post will add plus 1 to the count.
	// id: = count + 1
	showPost(posts);
	alert("Successfully added post.");
})

// Show Post

const showPost = (post) => {
	let postEntries = "";

	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	})

	document.querySelector("#div-post-entries").innerHTML = postEntries
}

// Edit Post

const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML
	let body = document.querySelector(`#post-body-${id}`).innerHTML

	document.querySelector("#txt-edit-id").value = id
	document.querySelector("#txt-title-edit").value = title
	document.querySelector("#txt-body-edit").value = body
}

// Update post

document.querySelector("#form-edit-post").addEventListener('submit', (event) =>{
	event.preventDefault()

	for (let i = 0; i < posts.length; i++) {
		 // The value posts[i].id is a Number while document.querySelector('#txt-edit-id').value is a String.
        // Therefore, it is necesary to convert the Number to a String first.

         // If match has been found, then proceed with assigning the new values to the existing post
		if(posts[i].id.toString() === document.querySelector("#txt-edit-id").value){
			posts[i].title = document.querySelector("#txt-title-edit").value
			posts[i].body = document.querySelector("#txt-body-edit").value

			showPost(posts)
			alert("Successfully edited post.")

			break

		}
	}
})

// Delete post

const deletePost = (id) => {
	posts = posts.filter((post) => {
		if (post.id.toString() !== id) {
			return post
		}
	})
	document.querySelector(`#post-${id}`).remove()
}